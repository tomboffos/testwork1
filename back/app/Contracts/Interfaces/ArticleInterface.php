<?php


namespace App\Contracts\Interfaces;


interface ArticleInterface
{
    const TITLE = 'title';
    const CONTENT = 'content';

    const FILLABLE = [
        self::TITLE,
        self::CONTENT
    ];
}
