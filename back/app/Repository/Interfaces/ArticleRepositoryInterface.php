<?php


namespace App\Repository\Interfaces;


use App\Http\Requests\General\Store\ArticleRequest;
use App\Models\Article;

interface ArticleRepositoryInterface
{
    public function index();
    public function store(ArticleRequest $request);
    public function update(ArticleRequest $request,Article $article);
    public function show(Article $article);
    public function destroy(Article $article);
}
