<?php


namespace App\Repository;


use App\Http\Requests\General\Store\ArticleRequest;
use App\Http\Resources\General\ArticleResource;
use App\Models\Article;
use App\Repository\Interfaces\ArticleRepositoryInterface;

class ArticleRepository implements ArticleRepositoryInterface
{
    public function index()
    {
        return ArticleResource::collection(Article::get());
    }

    public function store(ArticleRequest $request)
    {
        return response(new ArticleResource(Article::create($request->validated())), 201);
    }

    public function update(ArticleRequest $request, Article $article)
    {
        $article->update($request->validated());
        return response(new ArticleResource($article), 201);
    }

    public function show(Article $article)
    {
        return response(new ArticleResource($article));
    }

    public function destroy(Article $article)
    {
        $article->delete();
        return response(['message' => 'Article successfully deleted!']);
    }
}
